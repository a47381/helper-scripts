#!/bin/sh

printf "Stopping LDAP Server: "
systemctl stop slapd.service
printf "done.\n"

if [ -d /etc/ldap/slapd.d ]; then
	mv /etc/ldap/slapd.d /etc/ldap/slapd.d.$(openssl rand -hex 12)
fi

mkdir -p /etc/ldap/slapd.d

if [ -d /var/lib/ldap ]; then
    mv /var/lib/ldap /var/lib/ldap.$(openssl rand -hex 12)
fi

mkdir -p /var/lib/ldap

config_fn=$(mktemp)
data_fn=$(mktemp)

printf "Fetching config.ldif: "
curl -s -o "${config_fn}" "https://gitlab.com/ipb-gsr/helper-scripts/-/raw/main/slapd-init/config.ldif"
printf "done.\n"

printf "Fetching data.ldif: "
curl -s -o "${data_fn}" "https://gitlab.com/ipb-gsr/helper-scripts/-/raw/main/slapd-init/data.ldif"
printf "done.\n"

slapadd -n 0 -F /etc/ldap/slapd.d -l $config_fn
slapadd -n 1 -F /etc/ldap/slapd.d -l $data_fn

chown -R openldap:openldap /etc/ldap/slapd.d
chown -R openldap:openldap /var/lib/ldap

printf "Starting LDAP Server: "
systemctl start slapd.service
printf "done.\n"

rm "${config_fn}"
rm "${data_fn}"

